new Chart(document.getElementById("bar-chart-grouped"), {
    type: 'bar',
    data: {
      labels: ["Current Month", "Last Month", "3 Months prior", "4 Months prior"],
      datasets: [
        {
          label: "1st-7th",
          backgroundColor: "blue",
          data: [133,221,783,2478]
        }, {
          label: "8th-14th",
          backgroundColor: "green",
          data: [408,547,675,734]
        }, {
          label: "15th-22nd",
          backgroundColor: "yellow",
          data: [200,139,338,543]
        }, {
          label: "23rd-30th",
          backgroundColor: "grey",
          data: [108,347,475,234]
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'New Customers per Month'
      }
    }
});